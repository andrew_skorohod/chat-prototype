@extends('layouts.app')

@section('content')
<link href='{{ asset('css/chat.css') }}' rel='stylesheet' type='text/css'>
<div class="container bootstrap snippet">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="portlet portlet-default">
                <div class="portlet-heading">
                    <div class="portlet-title">
                        <h4><i class="fa fa-circle text-green"></i> {{ Auth::user()->name }}</h4>
                    </div>
                    <div class="portlet-widgets">
                        <span class="divider"></span>
                        <a data-toggle="collapse" data-parent="#accordion" href="#chat"><i class="fa fa-chevron-down"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div id="chat" class="panel-collapse collapse in">
                    <div>
                    <div class="portlet-body chat-widget" style="overflow-y: auto; width: auto; height: 300px;">
                        @foreach ($msgs as $msg)
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="media">
                                    <a class="pull-left" href="#">
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                    </a>
                                    <div class="media-body">
                                        <h4 class="media-heading">{{ $msg->user->name }}
                                            <span class="small pull-right">
                                                {{ date('h:i:s', strtotime($msg->created_at)) }}
                                            </span>
                                        </h4>
                                        <p>{{ $msg->text }}</p>
                                        @if ( $msg->file)
                                        <p>
                                            <a href="{{ url('/'.'uploads/'.$msg->id.'.txt')}}" target="_blank">
                                            <i class="fa fa-file" aria-hidden="true"></i>
                                            </a> 
                                        </p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        @endforeach


                    </div>
                    </div>
                    
                    <div class="portlet-footer">
                        <form enctype="multipart/form-data" action="{{ url('message') }}" method="POST" role="form" >
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input type="text" name="text" id="message-name" class="form-control">
                                <input type="file" accept=".txt" name="file" id="file">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-default" id="submitButton">
                                    <i class="fa fa-plus" ></i> Add Message
                                </button>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>        
@endsection