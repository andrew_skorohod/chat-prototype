$('#file').bind('change', function() {
  if (this.files[0].size > 102400){
      $( "#submitButton").attr('disabled',true);//disable button if size is bigger then 100 kb
      var foo = '<p class="bg-danger">File is too big.Max file size is 100 kb </p>';
      $('#file').append(foo);
  }
  else {//remove disabled attr
      $( "#submitButton").attr('disabled',false);
      $('.bg-danger').remove();
  }

});
