<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Message;

class MessageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {// get all msgs with info about their user
        $msgs = Message::with('user')->orderBy('created_at', 'desc')->get();

        return view('messages.chat',[
            'msgs' => $msgs,
        ]);
    }
    
public function store(Request $request)//save msg + file 
{
    if ($request->hasFile('file')) {
        $file = True;// need this field for displaying
        $msg = $request->user()->messages()->create([
            'text' => $request->text,
            'file' => $file,
        ]);
        $fileName = $msg->id . '.txt';// rename and save file
        $request->file('file')->move( base_path() .'\public\uploads',$fileName);
    }
    else {
        $file = False;
        $request->user()->messages()->create([
            'text' => $request->text,
            'file' => $file,
        ]);
    }

    return redirect('/messages');
}

public function destroy($id)//removed in final version
{
     Message::findOrFail($id)->delete();
     return redirect('/messages');
}
}