<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();// only registered users are able to see chat

Route::get('/home', 'HomeController@index');

Route::get('/messages', 'MessageController@index');//get all
Route::post('/message', 'MessageController@store');//add msg
Route::delete('/message/{task}', 'MessageController@destroy');//removed in final version

